//
//  ViewController.swift
//  Pokedex.GabrielGlatzCampos
//
//  Created by COTEMIG on 21/02/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLable: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonHabilidade.isEnable = false
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(clickNoLabel))
    }
    
    @objc func clickNoLabel(){
        print("Clicou na label")
    }
    
    @IBAction func clickPokedex(_ sender: Any){
        titleLable.text = "Clicou no Pokedex"
        titleLable.textColor = .cyan
        buttonHabilidades.isEnabled = true
    }
    
    @IBAction func clickMovimentos(_ sender: Any){
        print("Clicou no Movimento")
    }


}

